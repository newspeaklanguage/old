Newspeak3
'NS2JS Tests'
class JavascriptGenerationTesting usingPlatform: platform minitest: minitest jsGeneration: jsGeneration = NewspeakObject (
(* Tests for the JavascriptGeneration module.

   Copyright 2012 SAP AG.
   
   Licensed under the Apache License, Version 2.0 (the ''License''); you may not use this file except in compliance with the License.  You may obtain a copy of the License at  http://www.apache.org/licenses/LICENSE-2.0
*)|
	private TestContext = minitest TestContext.

	private Writer = jsGeneration Writer.
	private syntax = jsGeneration syntax.
	private js = jsGeneration factory.
|)
(
class NodeTests = TestContext (|
	node1
	node2
|)
('as yet unclassified'
testAssigmentComparisonEquals = (
	node1:: js assign: (js ident: 'foo') toBe: (js literal: 3).
	node2:: js assign: (js ident: 'foo') toBe: (js literal: 3).
	assert: node1 equals: node2
)
testAssigmentComparisonExprsNotEqual = (
	node1:: js assign: (js ident: 'foo') toBe: (js literal: 3).
	node2:: js assign: (js ident: 'foo') toBe: (js literal: 4).
	deny: node1 = node2
)
testAssigmentComparisonNamesNotEqual = (
	node1:: js assign: (js ident: 'foo') toBe: (js literal: 3).
	node2:: js assign: (js ident: 'bar') toBe: (js literal: 4).
	deny: node1 = node2
)
testBlockComparisonEquals = (
	node1:: syntax Block statements:
		{syntax AssignmentExpression var: 'foo' to: (syntax IntegerLiteral value: 3)}.
	node2:: syntax Block statements:
		{syntax AssignmentExpression var: 'foo' to: (syntax IntegerLiteral value: 3)}.
	assert: node1 equals: node2
)
testBlockComparisonNotEquals1 = (
	node1:: syntax Block statements:
		{syntax AssignmentExpression var: 'foo' to: (syntax IntegerLiteral value: 3)}.
	node2:: syntax Block statements:
		{syntax AssignmentExpression var: 'foo' to: (syntax IntegerLiteral value: 4)}.
	deny: node1 = node2
)
testBlockComparisonNotEquals2 = (
	node1:: syntax Block statements:
		{syntax AssignmentExpression var: 'foo' to: (syntax IntegerLiteral value: 3)}.
	node2:: syntax Block empty.
	deny: node1 = node2
)
testCallExpressionComparisonEquals = (
	node1:: syntax CallExpression
		function: (syntax IdentifierExpression name: 'foo')
		arguments: {syntax IntegerLiteral value: 3}.
	node2:: syntax CallExpression
		function: (syntax IdentifierExpression name: 'foo')
		arguments: {syntax IntegerLiteral value: 3}.
	assert: node1 equals: node2
)
testCallExpressionComparisonNotEqual1 = (
	node1:: syntax CallExpression
		function: (syntax IdentifierExpression name: 'foo')
		arguments: {syntax IntegerLiteral value: 3}.
	node2:: syntax CallExpression
		function: (syntax IdentifierExpression name: 'bar')
		arguments: {syntax IntegerLiteral value: 3}.
	deny: node1 = node2
)
testCallExpressionComparisonNotEqual2 = (
	node1:: syntax CallExpression
		function: (syntax IdentifierExpression name: 'foo')
		arguments: {syntax IntegerLiteral value: 3}.
	node2:: syntax CallExpression
		function: (syntax IdentifierExpression name: 'foo')
		arguments: {syntax IntegerLiteral value: 4}.
	deny: node1 = node2
)
testFunctionExpressionComparisonEquals = (
	node1:: syntax FunctionExpression
		name: 'foo'
		parameters: {'bar'}
		body: (syntax ReturnStatement expression: (syntax IntegerLiteral value: 3)) asBlock.
	node2:: syntax FunctionExpression
		name: 'foo'
		parameters: {'bar'}
		body: (syntax ReturnStatement expression: (syntax IntegerLiteral value: 3)) asBlock.
	assert: node1 equals: node2
)
testFunctionExpressionComparisonNotEqual1 = (
	node1:: syntax FunctionExpression
		name: 'foo'
		parameters: {}
		body: (syntax ReturnStatement expression: (syntax IntegerLiteral value: 3)) asBlock.
	node2:: syntax FunctionExpression
		name: 'foo'
		parameters: {'bar'}
		body: (syntax ReturnStatement expression: (syntax IntegerLiteral value: 3)) asBlock.
	deny: node1 = node2
)
testFunctionExpressionComparisonNotEqual2 = (
	node1:: syntax FunctionExpression
		name: 'foo'
		parameters: {}
		body: (syntax ReturnStatement expression: (syntax IntegerLiteral value: 3)) asBlock.
	node2:: syntax FunctionExpression
		name: 'foo'
		parameters: {'bar'}
		body: (syntax ReturnStatement expression: (syntax IntegerLiteral value: 3)) asBlock.
	deny: node1 = node2
)
testFunctionExpressionComparisonNotEqual3 = (
	node1:: syntax FunctionExpression
		name: 'foo'
		parameters: {'bar'}
		body: syntax Block empty.
	node2:: syntax FunctionExpression
		name: 'foo'
		parameters: {'bar'}
		body: (syntax ReturnStatement expression: (syntax IntegerLiteral value: 3)) asBlock.
	deny: node1 = node2
)
testIfStatementComparisonEquals = (
	node1:: syntax IfStatement
		expression: (syntax CallExpression function: (syntax IdentifierExpression name: 'foo') arguments: {})
		then: (syntax ReturnStatement expression: (syntax IntegerLiteral value: 3)) asBlock
		else: (syntax ReturnStatement expression: (syntax IntegerLiteral value: 4)) asBlock.
	node2:: syntax IfStatement
		expression: (syntax CallExpression function: (syntax IdentifierExpression name: 'foo') arguments: {})
		then: (syntax ReturnStatement expression: (syntax IntegerLiteral value: 3)) asBlock
		else: (syntax ReturnStatement expression: (syntax IntegerLiteral value: 4)) asBlock.
	assert: node1 equals: node2
)
testIfStatementComparisonNotEqual1 = (
	node1:: syntax IfStatement
		expression: (syntax CallExpression function: (syntax IdentifierExpression name: 'foo') arguments: {})
		then: (syntax ReturnStatement expression: (syntax IntegerLiteral value: 3)) asBlock
		else: (syntax ReturnStatement expression: (syntax IntegerLiteral value: 4)) asBlock.
	node2:: syntax IfStatement
		expression: (syntax CallExpression function: (syntax IdentifierExpression name: 'bar') arguments: {})
		then: (syntax ReturnStatement expression: (syntax IntegerLiteral value: 3)) asBlock
		else: (syntax ReturnStatement expression: (syntax IntegerLiteral value: 4)) asBlock.
	deny: node1 = node2
)
testIfStatementComparisonNotEqual2 = (
	node1:: syntax IfStatement
		expression: (syntax CallExpression function: (syntax IdentifierExpression name: 'foo') arguments: {})
		then: (syntax ReturnStatement expression: (syntax IntegerLiteral value: 3)) asBlock
		else: (syntax ReturnStatement expression: (syntax IntegerLiteral value: 4)) asBlock.
	node2:: syntax IfStatement
		expression: (syntax CallExpression function: (syntax IdentifierExpression name: 'foo') arguments: {})
		then: (syntax ReturnStatement expression: (syntax IntegerLiteral value: 3)) asBlock.
	deny: node1 = node2
)
testIfStatementComparisonNotEqual3 = (
	node1:: syntax IfStatement
		expression: (syntax CallExpression function: (syntax IdentifierExpression name: 'foo') arguments: {})
		then: (syntax ReturnStatement expression: (syntax IntegerLiteral value: 3)) asBlock
		else: (syntax ReturnStatement expression: (syntax IntegerLiteral value: 4)) asBlock.
	node2:: syntax IfStatement
		expression: (syntax CallExpression function: (syntax IdentifierExpression name: 'bar') arguments: {})
		then: (syntax ReturnStatement expression: (syntax IntegerLiteral value: 4)) asBlock
		else: (syntax ReturnStatement expression: (syntax IntegerLiteral value: 4)) asBlock.
	deny: node1 = node2
)
testIfStatementComparisonNotEqual4 = (
	node1:: syntax IfStatement
		expression: (syntax CallExpression function: (syntax IdentifierExpression name: 'foo') arguments: {})
		then: (syntax ReturnStatement expression: (syntax IntegerLiteral value: 3)) asBlock
		else: (syntax ReturnStatement expression: (syntax IntegerLiteral value: 4)) asBlock.
	node2:: syntax IfStatement
		expression: (syntax CallExpression function: (syntax IdentifierExpression name: 'bar') arguments: {})
		then: (syntax ReturnStatement expression: (syntax IntegerLiteral value: 3)) asBlock
		else: (syntax ReturnStatement expression: (syntax IntegerLiteral value: 3)) asBlock.
	deny: node1 = node2
)
testTryStatementComparisonEquals = (
	node1:: syntax TryStatement
		block: (syntax CallExpression function: (syntax IdentifierExpression name: 'foo') arguments: {}) asBlock
		catch: 'e'
		with: (syntax ReturnStatement expression: (syntax IntegerLiteral value: 3)) asBlock.
	node2:: syntax TryStatement
		block: (syntax CallExpression function: (syntax IdentifierExpression name: 'foo') arguments: {}) asBlock
		catch: 'e'
		with: (syntax ReturnStatement expression: (syntax IntegerLiteral value: 3)) asBlock.
	assert: node1 equals: node2
)
testTryStatementComparisonNotEqual1 = (
	node1:: syntax TryStatement
		block: (syntax CallExpression function: (syntax IdentifierExpression name: 'foo') arguments: {}) asBlock
		catch: 'e'
		with: (syntax ReturnStatement expression: (syntax IntegerLiteral value: 3)) asBlock.
	node2:: syntax TryStatement
		block: (syntax CallExpression function: (syntax IdentifierExpression name: 'bar') arguments: {}) asBlock
		catch: 'e'
		with: (syntax ReturnStatement expression: (syntax IntegerLiteral value: 3)) asBlock.
	deny: node1 = node2
)
testTryStatementComparisonNotEqual2 = (
	node1:: syntax TryStatement
		block: (syntax CallExpression function: (syntax IdentifierExpression name: 'foo') arguments: {}) asBlock
		catch: 'e'
		with: (syntax ReturnStatement expression: (syntax IntegerLiteral value: 3)) asBlock.
	node2:: syntax TryStatement
		block: (syntax CallExpression function: (syntax IdentifierExpression name: 'foo') arguments: {}) asBlock
		catch: 'ex'
		with: (syntax ReturnStatement expression: (syntax IntegerLiteral value: 3)) asBlock.
	deny: node1 = node2
)
testTryStatementComparisonNotEqual3 = (
	node1:: syntax TryStatement
		block: (syntax CallExpression function: (syntax IdentifierExpression name: 'foo') arguments: {}) asBlock
		catch: 'e'
		with: (syntax ReturnStatement expression: (syntax IntegerLiteral value: 3)) asBlock.
	node2:: syntax TryStatement
		block: (syntax CallExpression function: (syntax IdentifierExpression name: 'foo') arguments: {}) asBlock
		catch: 'e'
		with: (syntax ReturnStatement expression: (syntax IntegerLiteral value: 4)) asBlock.
	deny: node1 = node2
)) : ('as yet unclassified'
TEST_CONTEXT = (
))
class WriterTests = TestContext (
(* Tests of the Writer class of JavascriptGeneration. *)|
	writer = Writer new.
	node
|)
('as yet unclassified'
testWritingArrayExpressionEmpty = (
	node:: js array: {}.
	assertOutputIs: '[]'
)
testWritingArrayExpressionNotEmpty = (
	node:: js array: 
		{js literal: 3.
		js literal: 'foo'}.
	assertOutputIs: 
'[
	3,
	"foo"
]'
)
testWritingAssignmentExpression = (
	node:: js assign: (js ident: 'foo') toBe: (js ident: 'bar').
	assertOutputIs: 'foo = bar'
)
testWritingBlock = (
	node:: js block: 
		{js call: (js ident: 'foo') with: {}.
		js return: nil}.
	assertOutputIs: 
'{
	foo();
	return;
}'
)
testWritingCallExpression = (
	node:: js call: (js ident: 'foo') with: {js ident: 'bar'. js ident: 'baz'}.
	assertOutputIs: 'foo(bar, baz)'
)
testWritingForInStatement = (
	node::
		js for: 'foo' in: (js ident: 'foobar') do: 
			(js block: {
				js call: (js ident: 'fisk') with: {js ident: 'foo'}
			}).
	assertOutputIs: 
'for (foo in foobar) {
	fisk(foo);
}'
)
testWritingForStatement = (
	node::
		js for: (js var: 'i' value: (js literal: 0))
			while: (js operator: '<' with: (js ident: 'i') and: (js literal: 10))
			step: (js postfixOperator: '++' on: (js ident: 'i'))
			do: 
				(js block: {
					js call: (js ident: 'fisk') with: {js ident: 'i'}
				}).
	assertOutputIs: 
'for (var i = 0; i < 10; i++) {
	fisk(i);
}'
)
testWritingFunctionExpressionNoBody = (
	node:: js functionOf: {} body: (js block: {}).
	assertOutputIs: '(function () {})'
)
testWritingFunctionExpressionNoName = (
	node::
		js functionOf: {'foo'. 'bar'}
		body: (js block: 
			{js call: (js ident: 'foo') with: {js ident: 'bar'}}).
	assertOutputIs:
'(function (foo, bar) {
	foo(bar);
})'
)
testWritingFunctionExpressionWithName = (
	node::
		js function: 'Quux'
		of: {'foo'. 'bar'}
		body: (js block: 
			{js call: (js ident: 'foo') with: {js ident: 'bar'}}).
	assertOutputIs:
'function Quux (foo, bar) {
	foo(bar);
}'
)
testWritingIdentifierExpression = (
	node:: js ident: 'foobar'.
	assertOutputIs: 'foobar'
)
testWritingIfStatement = (
	node::
		js if: (js call: (js ident: 'foo') with: {})
		then: (js block: {
			js return: (js ident: 'bar')})
		else: (js block: {
			js return: (js ident: 'baz')}).
	assertOutputIs: 
'if (foo()) {
	return bar;
} else {
	return baz;
}'
)
testWritingIfStatementNoElse = (
	node::
		js if: (js call: (js ident: 'foo') with: {})
		then: (js block: {
			js return: (js ident: 'bar')}).
	assertOutputIs: 
'if (foo()) {
	return bar;
}'
)
testWritingIntegerLiteral = (
	node:: js literal: 42.
	assertOutputIs: '42'
)
testWritingMemberIdExpression = (
	node:: js propertyOf: (js ident: 'foo') at: (js ident: 'bar').
	assertOutputIs: 'foo[bar]'
)
testWritingMemberIntegerExpression = (
	node:: js propertyOf: (js ident: 'foo') at: (js literal: 5).
	assertOutputIs: 'foo[5]'
)
testWritingMemberStringExpression = (
	node:: js propertyOf: (js ident: 'foo') at: (js literal: 'bar').
	assertOutputIs: 'foo["bar"]'
)
testWritingNewExpression = (
	node:: js new: (js ident: 'Foo') with: {js ident: 'bar'. js ident: 'baz'}.
	assertOutputIs: 'new Foo(bar, baz)'
)
testWritingReturnStatement = (
	node:: js return: (js ident: 'foo').
	assertOutputIs: 'return foo'
)
testWritingReturnStatementNoValue = (
	node:: js return: nil.
	assertOutputIs: 'return'
)
testWritingStringLiteral = (
	node:: js literal: 'foobar'.
	assertOutputIs: '"foobar"'.
)
testWritingTryStatement = (
	node::
		js try:
			(js call: (js ident: 'foo') with: {}) asBlock
		catch:
			'bar' 
		with:
			(js return: (js ident: 'baz')) asBlock
		finally:
			(js call: (js ident: 'quux') with: {}) asBlock.
	assertOutputIs: 
'try {
	foo();
} catch (bar) {
	return baz;
} finally {
	quux();
}'
)
testWritingTryStatementNoFinally = (
	node:: js try:
			(js call: (js ident: 'foo') with: {}) asBlock
		catch:
			'bar' 
		with:
			(js return: (js ident: 'baz')) asBlock.
	assertOutputIs: 
'try {
	foo();
} catch (bar) {
	return baz;
}'
)
testWritingVariableNoInitializerStatement = (
	node:: js var: 'foo'.
	assertOutputIs: 'var foo'
)
testWritingVariableWithInitializerStatement = (
	node:: js var: 'foo' value: (js ident: 'bar').
	assertOutputIs: 'var foo = bar'
)'private'
protected assertOutputIs: expected <String> = (
	writer generateSourceFor: node.
	assert: writer contents equals: expected
)) : ('as yet unclassified'
TEST_CONTEXT = (
))) : ()
Smalltalk
package 'NewspeakSynchronization' = (

'NewspeakSynchronization'
class CanonicalRepresentation = Object (
"Canonical representation for synchronization. Objects answer an instance of this class in respond to the message ''canonicalRepresentation'' when their internal data structures are not suitable for reflective analysis for the purpose of object synchronization.

This class was designed to be used by collections like Set and Dictionary, so a synchronization mechanism can examine the elements of such data structures without detailed knowledge of the data-structure implementation.

Notes:

Collection has 7 subclasses:

* Bag
	For the purposes of synchronization, a bag is just an object with a dictionary, and the bag itself need no special treatment.

* CharacterSet, and Matrix
	For the purposes of synchronization, just objects with an array which has a deterministic size.

* SequenceableCollection
	Some of its subclasses need special treatment, at least: Heap, OrderedCollection, Semaphore, and SourceFileArray.
	Two OrderedCollection may hold the same elements but the size of their array may vary or their indices may differ.

* Set
	Set and its subclass Dictionary both require special treatment. This should be in place.

* SkipList
	Further analysis needed.

* WeakRegistry
	Further analysis needed.

Some collections has blocks which are used for sorting them.

What is the canonical representation of a block (BlockContext)?

   Copyright 2008 Cadence Design Systems, Inc.
   
   Licensed under the Apache License, Version 2.0 (the ''License''); you may not use this file except in compliance with the License.  You may obtain a copy of the License at  http://www.apache.org/licenses/LICENSE-2.0
"
| "instance variables" isOrdered elements originalClass  |
| "class pool variables"  |
| "shared pool variables"  |
'as yet unclassified'
= other = (
	self class == other class ifFalse: [^false].
	self isOrdered = other isOrdered ifFalse: [^false].
	self elements size = other elements size ifFalse: [^false].
	^isOrdered
		ifTrue: [
			self elements with: other elements do: [ :my :others |
				my == others ifFalse: [^false]].
			true]
		ifFalse: [self elements asIdentitySet = other elements asIdentitySet]
)
hash = (
	^elements inject: isOrdered hash into: [ :a :b | a bitXor: b identityHash]
)
printOn: s = (
	super printOn: s.
	s nextPut: $(.
	originalClass ifNotNil: [
		s nextPutAll: originalClass name; nextPut: $:.
		elements isEmptyOrNil ifFalse: [s nextPut: $ ]].
	elements do: [ :ea | ea printOn: s] separatedBy: [s nextPutAll: '. '].
	s nextPut: $).
)'accessing'
elements = (
	"Answer the value of elements"

	^ elements
)
elements: anObject = (
	"Set the value of elements"

	elements := anObject
)
isOrdered = (
	"Answer the value of isOrdered"

	^ isOrdered
)
isOrdered: anObject = (
	"Set the value of isOrdered"

	isOrdered := anObject
)
originalClass = (
	"Answer the value of originalClass"

	^ originalClass
)
originalClass: anObject = (
	"Set the value of originalClass"

	originalClass := anObject
)) : (
| "class instance variables"  |
'instance creation'
ordered: elements = (
	^self new isOrdered: true; elements: elements asArray
)
unordered: elements = (
	^self new isOrdered: false; elements: elements asArray
))

'NewspeakSynchronization-Tests'
class NewspeakSynchronizationCoreTest = NewspeakSynchronizationTestCase (
"Copyright 2008 Cadence Design Systems, Inc.
   
   Licensed under the Apache License, Version 2.0 (the ''License''); you may not use this file except in compliance with the License.  You may obtain a copy of the License at  http://www.apache.org/licenses/LICENSE-2.0
"
| "instance variables"  |
| "class pool variables"  |
| "shared pool variables"  |
'as yet unclassified'
testSnapshot = (

	self assert: [self newspeakSynchronization snapshot size = self newspeakSynchronization snapshot asIdentitySet size]
)
testTransactionID = (
	| t1 t2 |

	t1:= TransactionID authorFullName: 'Chewbacca' email: 'chewie@MillenniumFalcon.com'.
	t2:= t1 copy.

	self assert: [t1 == t1].
	self deny: [t1 == t2].
	self assert: [t1 = t2].
	self assert: [t1 hash = t2 hash].

	t2:= t1 deepCopy.
	self deny: [t1 == t2].
	self assert: [t1 = t2].
	self assert: [t1 hash = t2 hash].

	t2 timeStamp: DateAndTime yesterday.
	self deny: [t1 = t2].

	self assert: [{t1. t2} sort = {t2. t1}].
)) : (
| "class instance variables"  |
)

'NewspeakSynchronization-Tests'
class NewspeakSynchronizationTestCase = TestCase (
"Copyright 2008 Cadence Design Systems, Inc.
   
   Licensed under the Apache License, Version 2.0 (the ''License''); you may not use this file except in compliance with the License.  You may obtain a copy of the License at  http://www.apache.org/licenses/LICENSE-2.0
"
| "instance variables"  |
| "class pool variables"  |
| "shared pool variables"  |
'accessing'
newspeakSynchronization = (
	^NewspeakSynchronizationTestResource current newspeakSynchronization
)
nof = (
	^NewspeakSynchronizationTestResource current nof
)
platform = (
	^NewspeakSynchronizationTestResource current platform
)) : (
| "class instance variables"  |
'accessing'
resources = (
	^{MultiLanguageTestResource. NewspeakSynchronizationTestResource}
)'testing'
isAbstract = (
	^name = #NewspeakSynchronizationTestCase
))

'NewspeakSynchronization-Tests'
class NewspeakSynchronizationTestResource = TestResource (
"Copyright 2008 Cadence Design Systems, Inc.
   
   Licensed under the Apache License, Version 2.0 (the ''License''); you may not use this file except in compliance with the License.  You may obtain a copy of the License at  http://www.apache.org/licenses/LICENSE-2.0
"
| "instance variables" platform vmMirror nof newspeakSynchronization  |
| "class pool variables"  |
| "shared pool variables"  |
'as yet unclassified'
systemSnapshot = (

	^SystemSnapshotTestResource current snapshot
)'accessing'
newspeakSynchronization: anObject = (
	"Set the value of newspeakSynchronization"

	newspeakSynchronization := anObject
)
nof = (
	"Answer the value of nof"

	^ nof
)
nof: anObject = (
	"Set the value of nof"

	nof := anObject
)
platform = (
	"Answer the value of platform"

	^ platform
)
platform: anObject = (
	"Set the value of platform"

	platform := anObject
)
vmMirror = (
	"Answer the value of vmMirror"

	^ vmMirror
)
vmMirror: anObject = (
	"Set the value of vmMirror"

	vmMirror := anObject
)'running'
setUp = (
	platform:= BlackMarket new.
	vmMirror:= SqueakVmMirror usingPlatform: platform.
	nof:= NewspeakObjectFormat vmMirror: vmMirror usingPlatform: platform.
	newspeakSynchronization:= NewspeakSynchronization usingPlatform: platform usingObjectFormat: nof withSystemObjects: self systemSnapshot
)
tearDown = (
	NoModificationError initialize.
	platform:= nil.
	nof:= nil.
	newspeakSynchronization:= nil
)) : (
| "class instance variables"  |
'as yet unclassified'
resources = (
	^{SystemSnapshotTestResource}
))

'NewspeakSynchronization-Tests'
class SystemSnapshotTestResource = TestResource (
""
| "instance variables" snapshot  |
| "class pool variables"  |
| "shared pool variables"  |
'as yet unclassified'
setUp = (

	super setUp.
	snapshot:= NewspeakSynchronization systemSnapshot: BlackMarket new.
)'accessing'
snapshot = (
	"Answer the value of snapshot"

	^ snapshot
)
snapshot: anObject = (
	"Set the value of snapshot"

	snapshot := anObject
)) : (
| "class instance variables"  |
)

'NewspeakSynchronization'
class TransactionID = Object (
"Copyright 2008 Cadence Design Systems, Inc.
   
   Licensed under the Apache License, Version 2.0 (the ''License''); you may not use this file except in compliance with the License.  You may obtain a copy of the License at  http://www.apache.org/licenses/LICENSE-2.0
"
| "instance variables" uuid timeStamp authorFullName authorEmail  |
| "class pool variables"  |
| "shared pool variables"  |
'as yet unclassified'
<= other = (
	^timeStamp <= other timeStamp
)
= other = (
	^self class == other class
		and: [uuid = other uuid
		and: [timeStamp = other timeStamp
		and: [authorFullName = other authorFullName
		and: [authorEmail = other authorEmail]]]]
)
hash = (
	^uuid hash
)
initialize = (

	uuid:= UUID new.
	timeStamp:= DateAndTime now
)
printOn: s = ( "<WriteStream>"
	self class name printOn: s.
	s nextPut: $:.
	authorFullName printOn: s.
	s nextPut: $@.
	uuid printOn: s.
)'accessing'
authorEmail = (
	"Answer the value of authorEmail"

	^ authorEmail
)
authorEmail: anObject = (
	"Set the value of authorEmail"

	authorEmail := anObject
)
authorFullName = (
	"Answer the value of authorFullName"

	^ authorFullName
)
authorFullName: anObject = (
	"Set the value of authorFullName"

	authorFullName := anObject
)
timeStamp = (
	"Answer the value of timeStamp"

	^ timeStamp
)
timeStamp: anObject = (
	"Set the value of timeStamp"

	timeStamp := anObject
)
uuid = (
	"Answer the value of uuid"

	^ uuid
)
uuid: anObject = (
	"Set the value of uuid"

	uuid := anObject
)) : (
| "class instance variables"  |
'as yet unclassified'
authorFullName: authorFullName email: authorEmail = (
	^super new
		authorFullName: authorFullName;
		authorEmail: authorEmail
)
new = (
	self shouldNotImplement
))

extensions Dictionary = (
'*NewspeakSynchronization'
canonicalRepresentation = (
	^CanonicalRepresentation unordered: self associations
)) : (
'*NewspeakSynchronization'
fromCanonicalRepresentation: c = (
	| newInstance |
	newInstance := self new: c elements size.
	c elements do: [ :ea | newInstance at: ea key put: ea value].
	^newInstance
))

extensions ProtoObject = (
'*NewspeakSynchronization'
canonicalRepresentation = (
	^self
)) : (
'*NewspeakSynchronization'
fromCanonicalRepresentation: c = (
	^c
))

extensions Set = (
'*NewspeakSynchronization'
canonicalRepresentation = (
	^CanonicalRepresentation unordered: self asArray
)) : (
'*NewspeakSynchronization'
fromCanonicalRepresentation: c = (
	| newInstance |
	newInstance := self new: c elements size.
	c elements do: [ :ea | newInstance add: ea].
	^newInstance
))

)
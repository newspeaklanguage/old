Newspeak3
'NS2JS'
class JsKernel = NewspeakObject (
(* Kernel module for NS2JS implementation.

This code was derived in part by converting the Strongtalk Magnitude class to Newspeak. For that class, the Sun Microsystems copyright and BSD license below applies.

The class MutableString was written at SAP, as were the instance methods and overall structure of JsKernel, and so the SAP copyright and Apache license applies as well.

Copyright 2008 Cadence Design Systems, Inc.
   
Licensed under the Apache License, Version 2.0 (the ''License''); you may not use this file except in compliance with the License.  You may obtain a copy of the License at  http://www.apache.org/licenses/LICENSE-2.0

Copyright (c) 1995-2006 Sun Microsystems, Inc. ALL RIGHTS RESERVED.
Copyright 2008-2009 David Pennell, Stephen Pair, Gilad Bracha and other contributors.
Copyright 2011 Ryan Macnak

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, 
 this  list of conditions and the following disclaimer.

* Redistribution in binary form must reproduce the above copyright notice, 
this list of conditions and the following disclaimer in the documentation and/o other materials provided with the distribution.

Neither the name of Sun Microsystems or the names of contributors may 
be used to endorse or promote products derived from this software without 
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, 
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS 
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
DAMAGE


   Copyright 2012 SAP AG.
   
   Licensed under the Apache License, Version 2.0 (the ''License''); you may not use this file except in compliance with the License.  You may obtain a copy of the License at  http://www.apache.org/licenses/LICENSE-2.0


*)|
	Message = catchMessageClass. (* causes a DNU send which allows us to capture the class of Message *)
|)
(
class Magnitude = (
(* A Magnitude[M] is an object that has a linear relationship with objects of type M, such
that they can be compared using the relational operations <,>,<=,>=.

%note: The derived operations are carefully defined to use <
    and not = for the following reasons:
        - speed
        - use of = may be bad for subclasses with a problematic definition of =,
          such as floating point numbers.
*)|
|)
('comparing'
< other <M> ^<Boolean> = (


	self subclassResponsibility
)
<= other <M> ^<Boolean> = (


	(* %todo: when using inherited types, use the original definition:
		^(other < self) not
	*)
	^self < other or: [ self = other ]
)
> other <M> ^<Boolean> = (


	(* %todo: for inherited types use:
		^other < self
	*)
	^(self <= other) not
)
>= other <M> ^<Boolean> = (


	^(self < other) not
)
between: min <M> and: max <M> ^<Boolean> = (

	(* test whether the receiver is in the range min -> max, inclusive *)

	(* test the max case first since end of range overflow is generally more common *)
	^self <= max and: [ self >= min ]
)
compare: other <M>  ^<Int> = (

	(* Returns -1 if self < other, 0 if self = other, or 1 if self > other *)

	^self < other
		ifTrue: [ -1 ]
		ifFalse: [ self > other
							ifTrue: [ 1 ] ifFalse: [ 0 ]  ]
)
max: other <ARG> ^<Self | ARG> = (
		(* {where X is arg 1 of #< message of receiverType;
			where ARG <X> is arg 1} *)

	(* The guaranteed is safe because of the inference clause *)
	^self > ((* guaranteed <M>:\ *) other)
		ifTrue: [ self ]
		ifFalse: [ other ]
)
min: other <ARG> ^<Self | ARG> = (
		(* {where X is arg 1 of #< message of receiverType;
			where ARG <X> is arg 1} *)

	(* The guaranteed is safe because of the inference clause *)
	^self < ((* guaranteed <M> *) other)
		ifTrue: [ self ]
		ifFalse: [ other ]
)) : ('utility'
defaultSort ^<[:X:M | Boolean]> = (
	(* {where X is returnType of #new message of receiverType} *)
	(* return a sort predicate suitable for a sorted collection of M *)

	(* The guarantee is safe because of the inference clause *)
	^(* guaranteed <[X,M,^Boolean]> *)
		[ :m1 <Magnitude[M]> :m2 <M> | m1 <= m2 ]
))
class MutableString initialSize: n = (
(* A string whose contents (but not size) can change after it's been created. The contents are stored as an array of individual characters (which in the Javascript platform means an array of 1-character strings).


   Copyright 2012 SAP AG.
   
   Licensed under the Apache License, Version 2.0 (the ''License''); you may not use this file except in compliance with the License.  You may obtain a copy of the License at  http://www.apache.org/licenses/LICENSE-2.0

*)|
	private contentsArray <Array[String]> = Array new: n.
|)
('as yet unclassified'
, anotherString = (
	| newInstance |
	newInstance:: self class new: self size + anotherString size.
	newInstance
		replaceFrom: 1 to: self size with: self startingAt: 1;
		replaceFrom: self size + 1 to: self size + 1 + anotherString size with: anotherString startingAt: 1.
	^newInstance
)
asString ^<String> = (
	^String withAll: (self collect: [:each | each])
	(* this identity collect: converts undefineds in contentsArray to null characters *)
)
asSymbol ^<String> = (
	^self asString
)
at: index <Integer> = (
	^(contentsArray at: index) ifNil: [String null] 
)
at: index <Integer> put: character <String> = (
	contentsArray at: index put: character.
	^character
)
collect: aBlock = (
	^contentsArray collect: [:each | aBlock value: (each ifNil: [String null])]
)
copyFrom: start to: stop = (
(* Answer a new mutable string with a copy of the receiver contents between the two indices, inclusive. The range is not validated; the sender must ensure its correctness. *)
	| newInstance |
	newInstance:: self class new: stop - start + 1.
	newInstance
		replaceFrom: 1 to: newInstance size with: self startingAt: start.
	^newInstance
)
copyWithSize: newSize = (
	| newString |
	newString:: self class new: newSize.
	newString
		replaceFrom: 1
		to: (newString size min: self size)
		with: self
		startingAt: 1.
	^newString
)
do: aBlock = (
	contentsArray do:
		[:each | aBlock value: (each ifNil: [String null])]
)
first = (
	^self at: 1
)
includes: element <String> = (
	self do: [:each | each = element ifTrue: [^true]].
	^false
)
isString = (
	^true
)
last = (
	^self at: self size
)
printString = (
	^'''', self asString, ''''
)
replaceFrom: start to: stop with: replacement startingAt: indexInReplacement = (
(* Destructively modify the receiver by replacing the elements between the two indices (inclusive) with those of the replacement, starting at the specified index in the replacement. Ranges are not validated; the sender must ensure their correctness. *)
	| index delta |
	index:: start.
	delta:: indexInReplacement - start.
	[index <= stop] whileTrue:
		[self at: index put: (replacement at: index + delta).
		index:: index + 1]
)
size = (
	^contentsArray size
)) : ('as yet unclassified'
new = (
	^self initialSize: 0
)
new: n = (
	^self initialSize: n
)
new: size <Integer> withAll: character <String> = (
	| instance |
	instance:: self initialSize: size.
	1 to: size do: [:index | instance at: index put: character].
	^instance
)
withAll: string <String> = (
	| instance |
	instance:: self initialSize: string size.
	1 to: string size do: [:index | instance at: index put: (string at: index)].
	^instance
))
class Value = NSObject (
(* Some objects can not change after they are created. They are deeply immutable and known as value objects. A value object is globally unique, in the sense that no other object is equal to it.

An object o is a value object iff, under the assumption that o is a value object, it can be shown that:
-All its slots are immutable and contain value objects.
-Its enclosing objects are all value objects.
-Its class inherits from class Value and does not override its identity method (==).

Examples of such objects are numbers, booleans, characters, literal strings, symbols and module definitions.
*))
('as yet unclassified'
copy ^<Instance> = (
	^self
)'testing'
== otherObject = (
	^self = otherObject
)) : ()'as yet unclassified'
Block ^ <Block class> = (
(* The NS2JS compiler augments built in JS prototypes (e.g., Function) with the required behavior. So the code for the class is actually built into the compiler. Here we access the class the compiler creates by means of a dirty trick *)
	^[] class
)
Boolean ^ <Boolean class> = (
(* See False for gory details *)
	^true class superclass
)
Class = (
	^self class class superclass
)
False ^ <False class> = (
(* The classes True and False are fake, because all the logic has to be in Boolean because we are augmenting JS' boolean prototype. At this writing, True and False do not yet exist, and false class is really Boolean, but this will change *)
	^false class
)
Metaclass = (
	^self class class class
)
String ^ <String class> = (
(* See Block for comments on this style of implementation *)
	^'' class
)
True ^ <True class> = (
(* See False for gory details *)
	^true class
)
UndefinedObject = (
	^nil class
)
doesNotUnderstand: aMessage = (
	aMessage selector = #catchMessageClass ifTrue:
		[^aMessage class].
	^super doesNotUnderstand: aMessage
)) : ()